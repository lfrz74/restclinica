package com.mitocode.api;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

////JAX RS 2.1
@Path("/paciente")
public class PacienteAPI {
	
	@Inject
	private IPacienteService service;	
	private List<Paciente> pacientes;
	private Paciente paciente;
	
	//localhost:8080/MiniBlog/rest/publicacion/listar/4
	@GET
	@Path("/listar/{id}")
	@Produces("application/json")
	public Response listar(@PathParam("id") Integer id) {
		try {
			Paciente pac = new Paciente();
			pac.setIdPaciente(id);
			this.paciente = this.service.listarPorId(pac);			
		}catch(Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.OK).entity(paciente).build();
	}
	@GET
	@Path("/lista/")
	@Produces("application/json")
	public Response listar() {
		try {
			this.pacientes = this.service.listar();			
		}catch(Exception e) {
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		}
		return Response.status(Response.Status.OK).entity(pacientes).build();
	}
	
	@POST
	@Path("/registrar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response registrar(Paciente p) {
		try {
			int rpta = this.service.registrar(p);
			
		} catch (Exception e) {			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} 
		return Response.status(Response.Status.OK).entity(p).build();
	}

	@PUT 
	@Path("/modificar")
	@Produces("application/json")
	@Consumes("application/json")
	public Response modificar(Paciente p) {
		try {
			int rpta = this.service.modificar(p);
			
		} catch (Exception e) {			
			return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
		} 
		return Response.status(Response.Status.OK).entity(p).build();
	}
}
