package com.mitocode.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.jws.WebMethod;
import javax.jws.WebService;

import com.mitocode.dao.IPacienteDAO;
import com.mitocode.model.Paciente;
import com.mitocode.service.IPacienteService;

@WebService
public class PacienteServiceImpl implements IPacienteService, Serializable{

	@EJB
	private IPacienteDAO dao;
		
	@WebMethod
	@Override
	public int registrar(Paciente pac) throws Exception {
		int rpta = dao.registrar(pac);
		return rpta > 0 ? 1 : 0;
	}

	@WebMethod
	@Override
	public int modificar(Paciente pac) throws Exception {
		int rpta = dao.modificar(pac);
		return rpta > 0 ? 1 : 0;
	}

	@WebMethod
	@Override
	public List<Paciente> listar() throws Exception {
		return dao.listar();
	}

	@WebMethod
	@Override
	public Paciente listarPorId(Paciente t) throws Exception {
		return dao.listarPorId(t);
	}
}
